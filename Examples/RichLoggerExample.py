from RichLogger import RichLogger

logger = RichLogger.get_logger("zen", level=RichLogger.DEBUG)
logger.debug("Debug message")
logger.info("Info message")
logger.warning("Warn message")
logger.error("Error message")
logger.critical("Critical message")

logger2 = RichLogger.get_logger("Shawn", level=RichLogger.DEBUG)
logger2.debug("Debug message")
logger2.info("Info message")
logger2.warning("Warn message")
logger2.error("Error message")
logger2.critical("Critical message")
