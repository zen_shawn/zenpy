#!/usr/bin/python3
import datetime
import logging
import os
import time


class RichLogger:
    __instance = None
    __LOGGER_FILE_PATH = "{}/.Logger/log_{}.log".format(
        os.getenv("HOME"), datetime.datetime.now().strftime("%Y-%m-%d__%H_%M_%S"))
    __LOGGER_FORMATTER_VERBOSE = "%(asctime)s %(name)s %(pathname)s:%(lineno)d:%(funcName)s - %(levelname)s: %(message)s"
    __LOGGER_FORMATTER_SIMPLE = "%(asctime)s %(name)s %(levelname)s: %(message)s"

    logger_file = ""
    DEBUG = logging.DEBUG
    INFO = logging.INFO
    WARN = logging.WARN
    ERROR = logging.ERROR
    CRITICAL = logging.CRITICAL

    def __init__(self, out_log_path=None, file_log_level=logging.DEBUG, console_log_level=logging.INFO, fmt="Verbose"):
        """
        Initialize the singleton logger
        Args:
            out_log_path (str, optional): [description]. Defaults to "~/.Logger/log_[time].log".
            file_log_level (int, optional): [description]. Defaults to logging.DEBUG.
            console_log_level (int, optional): [description]. Defaults to logging.INFO.
            format (str, optional): "Verbose" or "Simple". Defaults to "Verbose".
        """
        if __class__.__instance is not None:
            return

        LOGGER_FORMATTER = __class__.__LOGGER_FORMATTER_VERBOSE if fmt == "Verbose" else __class__.__LOGGER_FORMATTER_SIMPLE
        if out_log_path is None:
            out_log_path = __class__.__LOGGER_FILE_PATH
        os.makedirs(os.path.dirname(out_log_path), exist_ok=True)
        logging.basicConfig(level=file_log_level,
                            format=LOGGER_FORMATTER,
                            datefmt='%Y-%m-%d %H:%M:%S',
                            filename=out_log_path,
                            filemode='a')
        __class__.logger_file = out_log_path
        try:
            import coloredlogs
            coloredlogs.install(logger=logging.getLogger(""),
                                level=console_log_level,
                                fmt=LOGGER_FORMATTER,
                                datefmt='%Y-%m-%d %H:%M:%S',
                                milliseconds=False)
        except ImportError:
            logging.warning("coloredlogs is not installed")
            console_logger = logging.StreamHandler()
            console_logger.setLevel(console_log_level)
            console_logger.setFormatter(logging.Formatter(
                LOGGER_FORMATTER, datefmt='%Y-%m-%d %H:%M:%S'))
            logging.getLogger('').addHandler(console_logger)
        __class__.__instance = self

    @ staticmethod
    def get_logger(name, level=logging.INFO):
        if __class__.__instance is None:
            __class__.__instance = RichLogger()
        logger = logging.getLogger(name)
        logger.setLevel(level)
        return logger


if __name__ == "__main__":
    def Test():
        RichLogger(fmt="Simple")
        logging.debug('Jackdaws love my big sphinx of quartz.')
        logger1 = RichLogger.get_logger('myapp.area1')
        logger2 = RichLogger.get_logger('myapp.area2')
        logger1.debug('Quick zephyrs blow, vexing daft Jim.')
        logger1.info('How quickly daft jumping zebras vex.')
        logger2.warning('Jail zesty vixen who grabbed pay from quack.')
        logger2.error('The five boxing wizards jump quickly.')
        logging.info(f"logger file written to: {RichLogger.logger_file}")

    Test()
