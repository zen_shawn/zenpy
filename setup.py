#!/usr/bin/python3
from setuptools import setup, find_packages

setup(name="Zenpy",
      version="0.0",
      author="Zen Shawn",
      author_email="xiaozisheng2008@qq.com",
      description="Useful tools for developping python programs",
      install_requires=['coloredlogs'],  # 指定项目最低限度需要运行的依赖项
      #   python_requires='>=2.6, !=3.0.*, !=3.1.*, !=3.2.*, <4',  # python的依赖关系
      python_requires='>=3.6',  # python的依赖关系
      packages=find_packages())
